# Code for "Neural Stochastic Differential Equations for Change Point Detection" paper

## Run experiments

```bash
cd scripts/
sh eval_baselines_all.sh # Obtain scores for baselines
sh train_sde_all.sh # Obtain scores for Neural SDE4CP
```




## Citation

Datasets and metrics were taken from [TCPD](https://github.com/alan-turing-institute/TCPDBench)

```bib
@article{vandenburg2020evaluation,
        title={An Evaluation of Change Point Detection Algorithms},
        author={{Van den Burg}, G. J. J. and Williams, C. K. I.},
        journal={arXiv preprint arXiv:2003.06222},
        year={2020}
}
```
