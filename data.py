import numpy as np
from torch.utils.data import DataLoader, Dataset
from sklearn.preprocessing import StandardScaler
from statsmodels.tsa.statespace.sarimax import SARIMAX
import pandas as pd


def sarima_transform(X):
    res = np.zeros_like(X)
    for dim in range(X.shape[-1]):
        if X[:, dim].std() == 0:
            continue
        model = SARIMAX(X[:, dim], order=(5, 1, 0))
        model_fit = model.fit(disp=0)
        res[:, dim] = model_fit.resid
    return res


class CPDataset(Dataset):
    def __init__(self, data, train=False, max_horizon=30, sarima=True, size=None):
        self.max_horizon = max_horizon
        self.train = train
        self.scaler = StandardScaler()
        if isinstance(data, str):
            self.X, self.gt = self.read_csv(data)
        else:
            self.X, self.gt = data
        if size:
            factor = int(np.ceil(self.X.shape[0]/size))
            self.X, self.gt = self.compress(self.X, self.gt, factor)
        self.original_X = self.X.squeeze()
        self.X = self.X.squeeze()
        if len(self.X.shape) == 1:
            self.X = np.expand_dims(self.X, -1)
        if sarima:
            self.X = sarima_transform(self.X)
        self.X = self.scaler.fit_transform(self.X)
        self.D = self.X.shape[-1]

    @staticmethod
    def read_csv(path):
        df = pd.read_csv(path)
        X = df[sorted([col for col in df.columns if 'X' in col])].values
        gt = {}
        for y_col in [col for col in df.columns if 'Label' in col]:
            values = np.argwhere(
                df[y_col].values == 1).squeeze().tolist()
            if isinstance(values, int):
                values = [values]
            gt[y_col.replace('Label', '')] = values
        return X, gt
    
    @staticmethod
    def compress(ts, gt, factor=1):
        def mov_avg(x, w, stride:int=1):
            res = []
            for m in range(int(np.ceil((len(x)-(w-1))/stride))):
                res.append(np.sum(x[m*stride:m*stride+w], 0) / w)
            return np.array(res)
        ts = mov_avg(ts, factor, factor)
        gt = {u:[int(x/factor) for x in v] for u, v in gt.items()}
        return ts, gt

    def __len__(self):
        return self.X.shape[0] - 1

    def __getitem__(self, idx):
        horizon = min(self.max_horizon, len(self)//5)
        if self.train:
            t0 = np.random.choice(range(len(self)-horizon))
            t1 = t0 + 1 + np.random.choice(np.arange(horizon))
        else:
            t0, t1 = idx, idx + 1
        X0, X1 = self.X[t0], self.X[t1]
        t0, t1 = [np.expand_dims(t, 0) for t in [t0, t1]]
        return t0, X0, t1, X1
