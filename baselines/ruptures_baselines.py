from ruptures import Binseg, BottomUp, Dynp, KernelCPD, Pelt, Window

from .dynpensembling import DynpEnsembling
from .windowensembling import WindowEnsembling
from .binsegensembling import BinsegEnsembling
from .bottomupensembling import BottomUpEnsembling
from ruptures.base import BaseEstimator

from sklearn.preprocessing import StandardScaler
import numpy as np

def __get_scores(algo: BaseEstimator, X:np.array, n_cps:int, max_number_of_cps=100, pelt=False):
    preds = np.zeros(X.shape[0])
    if n_cps is not None:
        my_bkps = algo.predict(n_bkps=n_cps)
        preds[my_bkps[:-1]] += 1
    else:
        for n_cps in range(1, max_number_of_cps):
            if not pelt:
                my_bkps = algo.predict(n_bkps=n_cps)
            else:
                my_bkps = algo.predict(pen=1./n_cps)
            preds[my_bkps[:-1]] += 1
    preds /= preds.max()
    return preds

def predict_win(X, model='l1', width=20, jump=1, n_cps=None):
    """
    n_cps - number of change points to return. If None then scores are returned
    """
    X = StandardScaler().fit_transform(X)
    algo = Window(model=model, width=width, jump=jump).fit(X)
    return __get_scores(algo, X, n_cps)

def predict_win_ensemble(X, model='l1', width=20, jump=1, ensembling=12, n_cps=None):
    X = StandardScaler().fit_transform(X)
    algo = WindowEnsembling(model=model, width=width, jump=jump, ensembling=ensembling).fit(X)
    return __get_scores(algo, X, n_cps)
    
def predict_binseg(X, custom_cost='mahalanobis', jump=1, n_cps=None):
    X = StandardScaler().fit_transform(X)
    algo = Binseg(custom_cost=custom_cost, jump=jump).fit(X)
    return __get_scores(algo, X, n_cps)
    
def predict_binseg_ensemble(X, custom_cost='mahalanobis', jump=1, n_cps=None, ensembling=12):
    X = StandardScaler().fit_transform(X)
    algo = BinsegEnsembling(custom_cost=custom_cost, jump=jump, ensembling=ensembling).fit(X)
    return __get_scores(algo, X, n_cps)

def predict_opt(X, custom_cost='mahalanobis', jump=5, n_cps=None): # Dynp
    X = StandardScaler().fit_transform(X)
    algo = Dynp(custom_cost=custom_cost, jump=jump).fit(X)
    return __get_scores(algo, X, n_cps)
    
def predict_opt_ensemble(X, custom_cost='mahalanobis', jump=5, n_cps=None, num_agg_func=12): # Dynp
    X = StandardScaler().fit_transform(X)
    algo = DynpEnsembling(custom_cost=custom_cost, jump=jump, ensembling=num_agg_func).fit(X)
    return __get_scores(algo, X, n_cps)

def predict_kcpd(X, n_cps=None, jump=5, kernel='rbf'):
    X = StandardScaler().fit_transform(X)
    algo = KernelCPD(kernel=kernel, jump=jump).fit(X)
    return __get_scores(algo, X, n_cps)

def predict_pelt(X, jump=5, n_cps=None):
    X = StandardScaler().fit_transform(X)
    algo = Pelt(jump=jump).fit(X)
    return __get_scores(algo, X, n_cps, pelt=True)