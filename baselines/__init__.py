r"""Search methods"""

from .neural_baselines import predict_tire, predict_klcpd
from .clasp import predict_clasp
from .bocpd import predict_bocpd
from .ruptures_baselines import *

ALGORITHMS = {
    'BOCPD': predict_bocpd,
    'ClaSP': predict_clasp,
    'TIRE': predict_tire,
    'KLCPD': predict_klcpd,
    'WIN': predict_win,
    'BINSEG': predict_binseg,
    'DYNP': predict_opt,
    'KERNEL': predict_kcpd,
    'PELT': predict_pelt,
#     'WIN_ENSEMBLE': predict_win_ensemble,
#     'BINSEG_ENSEMBLE': predict_binseg_ensemble,
#     'DYNP_ENSEMBLE': predict_opt_ensemble,
}

UNIVARIATE_ALGORITHMS = {'ClaSP', 'BOCPD'}