from sktime.annotation.clasp import ClaSPSegmentation
from sklearn.preprocessing import StandardScaler
import numpy as np

def predict_clasp(X,n_cps=1,**kwargs):
    X = StandardScaler().fit_transform(X)
    if X.shape[1] > 1: return None # multivariate
    model = ClaSPSegmentation(fmt='dense', n_cps=n_cps)
    model.fit(X[:, 0])
    preds = model.predict_scores(X[:, 0])
    cpd_scores = np.zeros(X.shape[0])
    cpd_scores[-preds.shape[0]:] = preds
    return cpd_scores