# https://github.com/hildensia/bayesian_changepoint_detection.git
from bayesian_changepoint_detection.hazard_functions import constant_hazard
from bayesian_changepoint_detection.bayesian_models import online_changepoint_detection
import bayesian_changepoint_detection.online_likelihoods as online_ll
from functools import partial
from sklearn.preprocessing import StandardScaler

def predict_bocpd(X):
    X = StandardScaler().fit_transform(X)
    hazard_function = partial(constant_hazard, 250)
    R, _ = online_changepoint_detection(
        X, hazard_function, online_ll.StudentT(alpha=0.1, beta=.01, kappa=1, mu=0)
    )
    Nw=10
    return R[Nw,:-1]
