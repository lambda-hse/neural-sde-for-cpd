from sklearn.preprocessing import StandardScaler
from TIRE import DenseTIRE as TIRE
import torch

def predict_tire(X,epoches=1000,**kwargs):
    X = StandardScaler().fit_transform(X)
    dim = X.shape[1]
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    model = TIRE(dim).to(device)
    model.fit(X, epoches=epoches)
    dissimilarities, change_point_scores = model.predict(X)
    return change_point_scores

from klcpd import KL_CPD

def predict_klcpd(X,epoches=100,**kwargs):
    X = StandardScaler().fit_transform(X)
    seq_length, dim = X.shape
    device = torch.device('cuda')
    model = KL_CPD(dim).to(device)
    model.fit(X, epoches=epoches)
    preds = model.predict(X);
    return preds
