def free_memory(globals_=False, locals_=True):
    for name in dir():
        if not name.startswith('_'):
            if globals_:
                del globals()[name]
            if locals_:
                del locals()[name]
    import gc; gc.collect();