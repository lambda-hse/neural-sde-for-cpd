import numpy as np

from tsmoothie.smoother import LowessSmoother
from sklearn.base import TransformerMixin, BaseEstimator

from scipy.signal import find_peaks, peak_prominences

# # Original TIRE implementation
# import warnings

# def _new_peak_prominences(scores):
#     with warnings.catch_warnings():
#         warnings.filterwarnings("ignore")
#         all_peak_prom = peak_prominences(scores,range(len(scores)))
#     return all_peak_prom

# def prominence_preprocessing(scores):
#     prominences = np.array(_new_peak_prominences(scores)[0])
#     prominences = prominences / np.amax(prominences)
#     return prominences

def prominence_preprocessing(x):
    x = x.copy()
    peaks, _ = find_peaks(x)
    prominences = peak_prominences(x, peaks)[0]
    res = np.zeros_like(x)
    res[peaks] = prominences
    return res
    
def get_indices(preds, agg_fn=lambda x: np.abs(x).max(1), margin=5, starting_points_to_ignore=2):        
    preds = agg_fn(preds)
    preds[0] = 0
    if margin == 'tire':
        preds = tire_preprocessing(preds)
    cpd_score = -preds
    change_points = np.argsort(cpd_score) + 1
    change_points = change_points[change_points > starting_points_to_ignore]
    res = []
    for point in change_points:
        if isinstance(margin, int) and any([abs(p1 - point) <= margin for p1 in res]):
            continue
        res.append(point)
    return res

def sarima_transform(X, order=(5,1,0)):
    res = np.zeros_like(X)
    for dim in range(X.shape[-1]):
        if X[:, dim].std() == 0:
            continue
        model = SARIMAX(X[:, dim], order=order)
        model_fit = model.fit(disp=0)
        res[:, dim] = model_fit.resid
    return res

def left_pad(X:np.array, n:int=1, v:float=0.):
    return np.array((v*np.zeros_like(X)[:n]).tolist() + X.tolist())

class DiffTransformer(TransformerMixin, BaseEstimator):
    def __init__(self, order=1, **kwargs):
        super().__init__()
        self.order = order
    
    def fit(self, X, y=None):
        self._is_fitted = True
        return self

    def transform(self, X, y=None):
        X_transformed = X
        for _ in range(self.order):
            X_transformed = left_pad(X_transformed[1:]-X_transformed[:-1])
        return X_transformed

    def fit_transform(self, X, y=None):
        return self.fit(X).transform(X)
    

class LowessSmoothTransformer(TransformerMixin, BaseEstimator, LowessSmoother):
    def fit(self, X, y=None):
        self._is_fitted = True
        return self

    def transform(self, X, y=None):
        self.smooth(X)
        D = X.shape[1]
        X = self.smooth_data
        if X.shape[0] == D: return X.transpose()
        return X

    def fit_transform(self, X, y=None):
        return self.fit(X).transform(X)