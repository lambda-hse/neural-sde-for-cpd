import io
import matplotlib.pyplot as plt
plt.rcParams.update({'font.size':35})
# import plotly.graph_objects as go
import numpy as np

from PIL import Image

def plt2pil(fig=None):
    """Convert a Matplotlib figure to a PIL Image and return it"""
    if fig is None:
        fig = plt.gcf()
    buf = io.BytesIO()
    fig.savefig(buf)
    buf.seek(0)
    img = Image.open(buf)
    return img

def plot(X, y=None, ts=None, xlabel='time', ylabel='signal', title='', margin=5, **kwargs):
    """
    X - 1D data
    y - labels (change points)s
    """
    plt.clf()
    plt.figure(figsize=(17,10))
    if ts is None:
        plt.plot(X, **kwargs);
    else:
        plt.plot(ts, X, **kwargs);
    plt.grid()
    if ylabel is not None:
        plt.ylabel(ylabel)
    if xlabel is not None:
        plt.xlabel(xlabel)

    if y is not None:
        for point in np.argwhere(y==1).squeeze(1):
            if ts is None:
                plt.axvspan(point-margin, point+margin, alpha=0.1, color='red', label='Change point')
            else:
                plt.axvspan(ts[point-margin], ts[point+margin], alpha=0.1, color='red', label='Change point')

    handles, labels = plt.gca().get_legend_handles_labels()
    by_label = dict(zip(labels, handles))
    plt.legend(by_label.values(), by_label.keys())