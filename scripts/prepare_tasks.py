import json
import os

DATASETS_REP_DIR = '../../datasets_cp/' # https://gitlab.com/lambda-hse/change-point/datasets repository
TASKS_PATH = 'tasks.json'

if __name__ == '__main__':
    data_dir = os.path.join(DATASETS_REP_DIR, 'data/')
    tasks = []
    for corpus_name in os.listdir(data_dir):
        corpus_path = os.path.join(data_dir, corpus_name)
        if corpus_name in {'tep','tssb', 'tcpd'}:
            continue
            for filename in os.listdir(corpus_path):
                if corpus_name == 'tep': dataset_name = 'tep'
                else: dataset_name = filename.split('.')[0]
                dataset_path = os.path.join(corpus_path, filename)
                if dataset_path.split('.')[-1] != 'csv': continue
                tasks.append({'corpus': corpus_name, 'dataset_name': dataset_name, 'dataset_path': os.path.abspath(dataset_path)})
        elif corpus_name == 'skab':
            continue
            for dirname in os.listdir(corpus_path):
                dirname_ = os.path.join(corpus_path, dirname)
                for filename in os.listdir(dirname_):
                    dataset_path = os.path.join(dirname_, filename)
                    if dataset_path.split('.')[-1] != 'csv': continue
                    tasks.append({'corpus': 'skab', 'dataset_name': f'{dirname}/{filename.split(".")[0]}', 'dataset_path': os.path.abspath(dataset_path)})
        else:
            for dataset_name in os.listdir(corpus_path):
                dataset_dir = os.path.join(corpus_path, dataset_name)
                if not os.path.isdir(dataset_dir): continue
                for sample in os.listdir(dataset_dir):
                    if corpus_name == 'synthetic':
                        if dataset_name not in {'synthetic_dataset_5', 'synthetic_dataset_7', 'synthetic_dataset_8', 'synthetic_dataset_9'}:
                            continue
                        if dataset_name == 'synthetic_dataset_8' and sample.split('.')[0] not in {'sample_10', 'sample_12', 'sample_13'}:
                            continue
                        elif dataset_name == 'synthetic_dataset_5' and sample.split('.')[0] != 'sample_0':
                            continue
                        elif dataset_name == 'synthetic_dataset_7' and sample.split('.')[0] != 'sample_5':
                            continue
                    dataset_path = os.path.join(corpus_path, dataset_name, sample)
                    if dataset_path.split('.')[-1] != 'csv': continue
                    tasks.append({'corpus': corpus_name, 'dataset_name': dataset_name, 'dataset_path': os.path.abspath(dataset_path)})
    with open(TASKS_PATH, 'w') as f:
        f.write(json.dumps(tasks, indent=4, sort_keys=True))
