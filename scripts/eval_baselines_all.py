import subprocess
from multiprocessing import Pool
import os
import json
import argparse
import random

TASKS_PATH = 'tasks.json'

def run_job(job):
    args = argparse.Namespace(**job['args'])
    task = job['task']
    execution_id = job['execution']
    job_id = f"{job['task_id']}_{execution_id}"
    if os.path.isfile(f"{args.results_dir}/{job_id}.json"):
        return
    cmd = ['python', 'eval_baselines.py', task['dataset_path'], job_id, '--dataset_desc', f"{task['corpus']}_{task['dataset_name']}", '--results_dir', args.results_dir]
    with open(f"{args.logs_dir}/{job_id}.log", "w") as log:
        p = subprocess.Popen(cmd, stderr=log)
        p.wait()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Script to run all the experiments')
    parser.add_argument('--results_dir', type=str, default='../results/')
    parser.add_argument('--logs_dir', type=str, default='./logs/')
    parser.add_argument('--executions_cnt', type=int, default=5, help='Number of executions for each experiment (to estimate uncertainty)')
    parser.add_argument('--n_threads', type=int, default=10, help='Number of threads')
    args = parser.parse_args()
    
    with open(TASKS_PATH) as f:
        tasks = json.load(f)
    
    jobs = []
    for task_id, task in enumerate(tasks):
        for execution in range(args.executions_cnt):
            jobs.append({'task': task, 'execution': execution, 'args': vars(args), 'task_id': task_id})
    random.shuffle(jobs)

    if args.n_threads == 1:
        for job in jobs:
            run_job(job)
    else:
        pool = Pool(args.n_threads)
        pool.map(run_job, jobs)
