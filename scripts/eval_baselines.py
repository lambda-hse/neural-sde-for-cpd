import wandb
wandb.init(project="sde_baselines")

import os
import pandas as pd
import numpy as np
import math
import matplotlib.pyplot as plt
import plotly.graph_objects as go

import sys; sys.path.append('../')
from baselines import ALGORITHMS as BASELINE_ALGORITHMS
from baselines import UNIVARIATE_ALGORITHMS
from utils.metrics import EMAMetric, covering, f_measure, evaluating_change_point, rcpd_score
from utils.visualization import plt2pil, plot
import json

import argparse

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Script to run CPD baselines for the experiments')
    parser.add_argument('dataset_path', type=str)
    parser.add_argument('id', type=str, help='Name of the run')
    parser.add_argument('--dataset_desc', type=str, default='')
    parser.add_argument('--results_dir', type=str, default='../results/')

    args = parser.parse_args()
    wandb.config.update(args)
    
    if not os.path.isdir(args.results_dir): os.mkdir(args.results_dir)
    
    plot_args = {'linewidth': 3}

    
    models_metrics = {}

    df = pd.read_csv(args.dataset_path)
    
    ts, X, y = 2*math.pi*np.arange(df.shape[0])/df.shape[0], df[[col for col in df.columns if 'X' in col]].values, df[[col for col in df.columns if 'Label' in col]].max(1).values

    D = X.shape[1]
    
    ts_index = df.Time
    if ts_index.dtype == int:
        ts_index = pd.to_datetime(ts_index, unit='seconds')
    else:
        ts_index = pd.to_datetime(ts_index)
    
    # Visualize data
    for d in range(D):
        plot(X[:, d], y, ts=None, xlabel='time', ylabel='signal', title='', margin=5, alpha=0.8, **plot_args)
        wandb.log({f"Data/original_data (dim {d})": wandb.Image(plt2pil())})


    def nab_score(preds, y_true=y):
        y_true = pd.Series(y, index=ts_index)
        evals = []
        for threshold in sorted(set(preds)):
            preds_rounded = pd.Series(np.where(preds>=threshold, np.ones_like(preds), np.zeros_like(preds)), ts_index)
            evals.append(evaluating_change_point(y_true, preds_rounded))
        return {k:max([x[k] for x in evals]) for k in evals[0]}

    def eval_(scores: np.array, y):
        cp_locations = np.argsort(-scores)
        gt = {0: np.argwhere(y>0).squeeze(1).tolist()}
        cov = max([covering(gt, cp_locations[:i], ts.shape[0]) for i in range(cp_locations.shape[0])])
        f = max([f_measure(gt, cp_locations[:i]) for i in range(cp_locations.shape[0])])
        rcpd = rcpd_score(y, scores)
        nab = nab_score(scores)
        return {'F1': f, 'Covering': cov, 'RCPD': rcpd, 'NAB': nab}

    for baseline_name, baseline in BASELINE_ALGORITHMS.items():
        if baseline_name in UNIVARIATE_ALGORITHMS and D > 1: continue
        preds = baseline(X)
        # Visualize preds
        plot(preds, y, ts=None, xlabel='time', ylabel='CPD score', title='', margin=5, alpha=0.8, **plot_args)
        wandb.log({f"{baseline_name}/preds": wandb.Image(plt2pil())})
        # Eval preds
        scores = eval_(preds, y)
        models_metrics[baseline_name] = scores
    
    for metric in {'F1', 'Covering', 'RCPD', 'NAB'}:
        names = list(models_metrics.keys())
        if metric != 'NAB':
            values = [models_metrics[name][metric] for name in names]
            plt.clf()
            if metric != 'RCPD':
                indices = np.argsort(values) # sort leaderboard by ascending
            else:
                indices = np.argsort(-np.array(values)) # sort leaderboard by descending
            fig = go.Figure(go.Bar(
                    x=np.array(values)[indices],
                    y=np.array(names)[indices],
                    orientation='h', opacity=0.5))
#             fig.update_layout(
#                 autosize=False,
#                 width=800,
#                 height=800,)
            wandb.log({f'Leaderboard/{metric}': fig})
        else: # NAB
            for nab_profile in ['LowFP', 'LowFN', 'Standart']:
                values = [models_metrics[name]['NAB'][nab_profile] for name in names]
                plt.clf()
                indices = np.argsort(values) # sort leaderboard by ascending
                fig = go.Figure(go.Bar(
                    x=np.array(values)[indices],
                    y=np.array(names)[indices],
                    orientation='h', opacity=0.5))
#                 fig.update_layout(
#                     autosize=False,
#                     width=800,
#                     height=800,)
                wandb.log({f'Leaderboard/NAB.{nab_profile}': fig})

        
    results_dict = {
        'url': wandb.run.url,
        'results': models_metrics,
        'dataset_path': args.dataset_path,
        'args': vars(args)
    }

    with open(os.path.join(args.results_dir, args.id + ".json"), 'w') as f:
        f.write(json.dumps(results_dict, sort_keys=True, indent=4))
    
