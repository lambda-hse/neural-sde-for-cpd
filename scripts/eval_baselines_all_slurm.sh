#!/bin/bash

N_THREADS=4
for thread in $(seq 1 $N_THREADS); do
    sbatch --gpus=1 --cpus-per-task 11 --error="baseline_slurm_logs/$thread.err" --output="slurm_logs/$thread.out" --job-name="CPD-baselines-$thread" eval_baselines_all.sh "$@" &
done

wait
