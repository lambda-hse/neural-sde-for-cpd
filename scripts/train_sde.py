import wandb
wandb.init(project="sde", entity="holybayes")


import argparse
import os
import sys
sys.path.append('../')
from utils.preprocessing import prominence_preprocessing, DiffTransformer, LowessSmoothTransformer
from utils.visualization import plot, plt2pil
from utils.init import manual_seed
from utils.optim import LinearScheduler, Ranger
from utils.metrics import EMAMetric, covering, f_measure, evaluating_change_point, rcpd_score
from collections import defaultdict

from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.pipeline import make_pipeline

from models import LatentSDE
import json
import pandas as pd
from glob import glob
import math
import matplotlib.pyplot as plt
plt.rcParams.update({'font.size':35})


import numpy as np
import torch
from tqdm.auto import tqdm
from torch import distributions, nn, optim
from models import LatentSDE
import torchsde

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Script to run SDE for CP experiments')
    parser.add_argument('dataset_path', type=str)
    parser.add_argument('id', type=str, help='Name of the run')
    parser.add_argument('--dataset_desc', type=str, default='')
    parser.add_argument('--results_dir', type=str, default='../sde_results/')
    parser.add_argument('--train_iters', type=int, default=100, help='Number of train steps for our SDE-based CPD algorithm')
    parser.add_argument('--kl_anneal_iters', type=int, default=80, help='Number of steps for KL factor annealing')   
    parser.add_argument('--postprocessing', type=bool, default=True, help='Postprocess CPD score of our SDE-based CPD algorithm')
    parser.add_argument('--batch_size', type=int, default=512)
    parser.add_argument('--likelihood', type=str, default='normal', choices=['normal', 'laplace'])
    parser.add_argument('--scale', type=float, default=0.5, help='Scale for likelihood')
    parser.add_argument('--lag', type=int, default=5, help='Lag for our SDE-based CPD algorithm')
    parser.add_argument('--origin_smooth', type=float, default=1e-2, help='Smooth for original data')
    parser.add_argument('--diff_smooth', type=float, default=4e-2, help='Smooth for 1st diff data')
    parser.add_argument('--seed', type=int, default=5)
    parser.add_argument('--opt', type=str, choices=['adam'], default='adam')
    
    parser.add_argument('--lr', type=float, default=1e-2)
    parser.add_argument('--n_pos_encodings', type=int, default=5, help='Number of positional encodings for our SDE-based CPD algorithm')
    parser.add_argument('--margin', type=int, default=5, help='Allowed margin between predicted and real change points. Used in F1 and Covering metrics')

    args = parser.parse_args()
    
    wandb.config.update(args)
    

    plot_args = {'linewidth': 3}
    

    df = pd.read_csv(args.dataset_path)
    df.Time = np.arange(df.shape[0])
    ts, X, y = 2*math.pi*df.Time.values/df.Time.max(), df[[col for col in df.columns if 'X' in col]].values, df[[col for col in df.columns if 'Label' in col]].max(1).values

    ts_index = df.Time
    if ts_index.dtype == int:
        ts_index = pd.to_datetime(ts_index, unit='seconds')
    else:
        ts_index = pd.to_datetime(ts_index)

    def nab_score(preds, y_true=y):
        y_true = pd.Series(y, index=ts_index)
        evals = []
        for threshold in sorted(set(preds)):
            preds_rounded = pd.Series(np.where(preds>=threshold, np.ones_like(preds), np.zeros_like(preds)), ts_index)
            evals.append(evaluating_change_point(y_true, preds_rounded))
        return {k:max([x[k] for x in evals]) for k in evals[0]}

    D = X.shape[1]

    for d in range(D):
        plot(X[:, d], y, ts=None, xlabel='time', ylabel='signal', title='', margin=5, alpha=0.8, **plot_args)
        wandb.log({f"Data/original_data (dim {d})": wandb.Image(plt2pil())})

    preprocessor = make_pipeline(LowessSmoothTransformer(args.origin_smooth), StandardScaler())
    for d in range(D):
        plot(preprocessor.fit_transform(X)[:, d], y, ts, xlabel='time', ylabel='signal', title='', margin=5, alpha=0.8, **plot_args)
        wandb.log({f"Data/standartized_data (dim {d})": wandb.Image(plt2pil())})

    diff_preprocessor = make_pipeline(DiffTransformer(order=1), LowessSmoothTransformer(args.diff_smooth), StandardScaler())
    for d in range(D):
        plot(diff_preprocessor.fit_transform(X)[:, d], y, ts, xlabel='time', ylabel='signal', title='', margin=5, alpha=0.8, **plot_args)
        wandb.log({f"Data/first_diff (dim {d})": wandb.Image(plt2pil())})


    ## Train SDE for change point detection (our algorithm)

    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    manual_seed(args.seed)

    X = np.hstack([preprocessor.fit_transform(X) for preprocessor in [preprocessor, diff_preprocessor]]) # bring first diff and original data together
    # X = preprocessor.fit_transform(X)

    ts, X = [torch.from_numpy(x).to(device).float() for x in [ts, X]]

    bm = torchsde.BrownianInterval(
        t0=ts[0],
        t1=ts[-1],
        size=(args.batch_size, 1),
        device=device,
        levy_area_approximation='space-time'
    )  # We need space-time Levy area to use the SRK solver

    # Model.
    input_size = X.shape[1]
    model = LatentSDE(input_size, n_pos_encodings=args.n_pos_encodings).to(device)
    optimizer = {'adam': optim.Adam}[args.opt](model.parameters(), lr=args.lr)
    scheduler = optim.lr_scheduler.ExponentialLR(optimizer, gamma=.999)
    kl_scheduler = LinearScheduler(iters=args.kl_anneal_iters)

    logpy_metric = EMAMetric()
    kl_metric = EMAMetric()
    loss_metric = EMAMetric()

    import warnings
    warnings.filterwarnings('ignore')

    wandb.watch(model, criterion=None, log="all", log_freq=100, idx=None,
        log_graph=(False))

    tbar = tqdm(range(args.train_iters), desc='???', position=0, leave=True)
    plt.figure(figsize=(17,10))

    best_f, best_cov, best_rcpd, best_nab = 0., 0., np.inf, defaultdict(float)

    for global_step in tqdm(range(args.train_iters)):
        # Training
        optimizer.zero_grad()
        zs, kl = model(ts=ts, batch_size=args.batch_size, y0=X[0])
        batch_size = zs.shape[0]

        likelihood_constructor = {"laplace": distributions.Laplace, "normal": distributions.Normal}[args.likelihood]

        train_indices = np.arange(X.shape[0])

        loss = torch.tensor(0.).to(device)

        logpys = []

        for d in range(input_size):
            likelihood = likelihood_constructor(loc=zs[:, :, d], scale=args.scale)
            logpy = likelihood.log_prob(X[train_indices, d:d+1]).mean(dim=1)
            logpys.append(logpy.detach().cpu().numpy())
            loss -= logpy.sum(dim=0)

        logpy_metric.step(loss)
        kl_metric.step(kl)
        loss += kl * kl_scheduler.val
        loss_metric.step(loss)
        wandb.log({'Train/loss': loss_metric.val,
                   'Train/logpy': logpy_metric.val,
                   'Train/kl': kl_metric.val,
                   'Train/kl_factor': kl_scheduler.val})

        loss.backward()

        logpys = np.array(logpys).transpose()

        optimizer.step()
        scheduler.step()
        kl_scheduler.step()

        t = ts.detach().cpu().numpy()

        latent_means, latent_variances = zs.mean(1).detach().cpu().numpy(),  zs.std(1).detach().cpu().numpy()

        def norm_pdf(x, mu, sigma, log=True):
            if log:
                return -(x-mu)**2/(2*sigma**2) - np.log(sigma*np.sqrt(2*np.pi))
            else:
                return np.exp(-(x-mu)**2/(2*sigma**2))/(sigma*np.sqrt(2*np.pi))

        data = X.detach().cpu().numpy()
        cpd_score = np.zeros_like(data)

        for lag in range(args.lag):
            cpd_score += norm_pdf(data, latent_means, latent_variances)
            cpd_score[:lag] = 0
            cpd_score[lag:] -= norm_pdf(data[lag:], latent_means[:latent_means.shape[0]-lag], latent_variances[:latent_variances.shape[0]-lag])

    #     cpd_score = np.exp(cpd_score)
    #     cpd_score = (cpd_score > 0) * cpd_score
        cpd_score = np.abs(cpd_score)


    #     cpd_score = cpd_score * (cpd_score > 0)
    #     cpd_score = (cpd_score - cpd_score.min(0))/(cpd_score.max(0)-cpd_score.min(0))
        if args.postprocessing:
            for i in range(cpd_score.shape[1]):
                cpd_score[:, i] = prominence_preprocessing(cpd_score[:, i])


        cpd_score_agg = cpd_score.max(1)
                
        def visualize_sde():
            # Visualization
            for d in range(D):
                std_width = 1.
                plt.clf()
                plt.plot(t, X[train_indices, d].squeeze().detach().cpu().numpy(), alpha=0.5, label='signal', **plot_args)
                sde_pred = zs[:, :, d].squeeze().detach().cpu().numpy()
                plt.plot(t, sde_pred.mean(1), alpha=0.5, label='SDE', c='orange', **plot_args)
                plt.fill_between(t, sde_pred.mean(1)-std_width*sde_pred.std(1), sde_pred.mean(1)+std_width*sde_pred.std(1), facecolor='orange', alpha=0.1, **plot_args)
                for point in np.argwhere(y==1).squeeze(1):
                    plt.axvspan(t[point-args.margin], t[point+args.margin], alpha=0.1, color='red', label='Change point')

                handles, labels = plt.gca().get_legend_handles_labels()
                by_label = dict(zip(labels, handles))
                plt.legend(by_label.values(), by_label.keys())
                plt.grid()
                plt.ylabel("signal")
                plt.xlabel("time")
                plt.ylim(-3,3)
                wandb.log({f"Train/SDE (preprocessed data, dim {d})": wandb.Image(plt2pil())})

                plt.clf()
                plt.plot(t, X[train_indices, D+d].squeeze().detach().cpu().numpy(), alpha=0.5, label='signal', **plot_args)
                sde_pred = zs[:, :, D+d].squeeze().detach().cpu().numpy()
                plt.plot(t, sde_pred.mean(1), alpha=0.5, label='SDE', c='orange', **plot_args)
                plt.fill_between(t, sde_pred.mean(1)-std_width*sde_pred.std(1), sde_pred.mean(1)+std_width*sde_pred.std(1), facecolor='orange', alpha=0.1, **plot_args)
                for point in np.argwhere(y==1).squeeze(1):
                    plt.axvspan(t[point-args.margin], t[point+args.margin], alpha=0.1, color='red', label='Change point')

                handles, labels = plt.gca().get_legend_handles_labels()
                by_label = dict(zip(labels, handles))
                plt.legend(by_label.values(), by_label.keys())
                plt.grid()
                plt.ylim(-3,3)
                plt.ylabel("signal")
                plt.xlabel("time")

                wandb.log({f"Train/SDE (1st diff, dim {d})": wandb.Image(plt2pil())})

            for d in range(cpd_score.shape[1]):
                plot(cpd_score[:, d], y, t, xlabel='time', ylabel=None, title='', margin=5, alpha=0.5, label='CPD score', **plot_args)

                plt.ylim(0, 1)
                wandb.log({f"Preds/CPD score ({d})": wandb.Image(plt2pil())})
                
            plot(cpd_score_agg, y, t, xlabel='time', ylabel=None, title='', margin=5, alpha=0.5, label='CPD score', **plot_args)
            plt.ylim(0,1)
            wandb.log({f"Preds/CPD score": wandb.Image(plt2pil())})



        cp_locations = np.argsort(-cpd_score_agg)
        gt = {0: np.argwhere(y>0).squeeze(1).tolist()}
        cov = max([covering(gt, cp_locations[:i], ts.shape[0]) for i in range(cp_locations.shape[0])])
        f = max([f_measure(gt, cp_locations[:i]) for i in range(cp_locations.shape[0])])

        rcpd = rcpd_score(y, cpd_score_agg)
        nab = nab_score(cpd_score_agg)
        
        if any([cov > best_cov, f > best_f, rcpd < best_rcpd]+[v>best_nab[k] for k,v in nab.items()]):
            visualize_sde()


        if cov > best_cov:
            best_cov = cov
            # Save checkpoints
        if f > best_f:
            best_f = f
        if rcpd < best_rcpd:
            best_rcpd = rcpd
        for k, v in nab.items():
            if v > best_nab[k]:
                best_nab[k] = v


        wandb.log({
            'Metrics/F1': f,
            'Metrics/Covering': cov,
            'Metrics/RCPD': rcpd,
            'Metrics/NAB': nab,
            'Metrics/Best_F1': best_f,
            'Metrics/Best_Covering': best_cov,
            'Metrics/Best_RCPD': best_rcpd,
            'Metrics/Best_NAB': best_nab
        })


        tbar.set_description(f"Best F1: {best_f:.3f}\t Best Cov: {best_cov:.3f}\t Best RCPD: {best_rcpd:.3f}\t Best NAB.LowFP: {best_nab['LowFP']:.3f}\t Best NAB.LowFN: {best_nab['LowFN']:.3f}\t Best NAB.Standart: {best_nab['Standart']:.3f}")
        tbar.refresh() # to show immediately the update

    results_dict = {
        'url': wandb.run.url,
        'results': {'F1': best_f, 'Covering': best_cov, 'RCPD': best_rcpd, 'NAB': best_nab},
        'dataset_path': args.dataset_path,
        'args': vars(args)
    }

    with open(os.path.join(args.results_dir, args.id + ".json"), 'w') as f:
        f.write(json.dumps(results_dict, sort_keys=True, indent=4))
    
