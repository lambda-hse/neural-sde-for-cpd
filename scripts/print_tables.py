import json
from tabulate import tabulate
import argparse
import os
from collections import defaultdict
import numpy as np

BASELINE_RESULTS_DIR = '../baseline_synthetic_results/'
SDE_RESULTS_DIR = '../sde_synthetic_results/'
DATASET_DIR = '../../datasets_cp/' # https://gitlab.com/lambda-hse/change-point/datasets
with open(os.path.join(DATASET_DIR, 'data/synthetic/description.json'), 'r') as f:
   SYNTHETIC_DATASETS_DESC = json.load(f)

class COLOR:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   END = '\033[0m'

def colorize(text, color):
    return color + str(text) + COLOR.END

def bold(text):
    return colorize(text, COLOR.BOLD)

parser = argparse.ArgumentParser(description='Script to print tables of results')
parser.add_argument('--corpus', type=str, default='all', choices=['all', 'synthetic', 'tcpd', 'tssb', 'skab', 'tep'],
                    help='Corpus')
parser.add_argument('--metric', type=str, default='all', 
                    choices=['all', 'nab.s', 'nab.fp', 'nab.fn', 
                                                 'tssb', 'f1', 'covering'],
                    help='Metric')
parser.add_argument('--aggregation', type=str, default='all', choices=['no', 'corpus', 'all'], 
                    help='No aggregation/average over corpus/average over all samples')
parser.add_argument('--dim', type=str, default='all', choices=['all', 'univariate', 'multivariate'],
                    help='Data dimensionality')
parser.add_argument('--uncertainty', type=bool, default=False, 
                    help='Estimate uncertainty or not')
parser.add_argument('--table_format', type=str, default='simple', choices=['simple', 'latex'])
args = parser.parse_args()

results = defaultdict(lambda: defaultdict(list)) # corpus_name -> dataset_name -> [algorithm_name -> metric_name -> values]
sde_results = defaultdict(lambda: defaultdict(list)) # corpus_name -> dataset_name -> metric_name -> values

for result_path in os.listdir(BASELINE_RESULTS_DIR):
    with open(os.path.join(BASELINE_RESULTS_DIR, result_path), 'r') as f:
        result = json.load(f)
    corpus_name = result['args']['dataset_desc'].split('_')[0]
    dataset_name = '_'.join(result['args']['dataset_desc'].split('_')[1:])
    dataset_path = result['args']['dataset_path']
    if 'synthetic' in corpus_name:
        dataset_name += '_' + 'sample' + '_' + dataset_path.split('.')[0].split('_')[-1]
    results[corpus_name][dataset_name].append(result['results'])

for results_path in os.listdir(SDE_RESULTS_DIR):
    with open(os.path.join(SDE_RESULTS_DIR, results_path), 'r') as f:
        result = json.load(f)
    corpus_name = result['args']['dataset_desc'].split('_')[0]
    dataset_name = '_'.join(result['args']['dataset_desc'].split('_')[1:])
    dataset_path = result['args']['dataset_path']
    if 'synthetic' in corpus_name:
        dataset_name += '_' + 'sample' + '_' + dataset_path.split('.')[0].split('_')[-1]
    sde_results[corpus_name][dataset_name].append(result['results'])

rows, headers = [], []
if args.aggregation == 'all':
   metric_names = ['NAB.Standart', 'NAB.LowFP', 'NAB.LowFN', 'F1', 'Covering', 'RCPD']
   headers = ['Algorithm'] + metric_names
   scores = defaultdict(lambda: defaultdict(list))# algorithm_name -> metric_name -> metric_scores
   if args.dim == 'multivariate':
      algorithm_names = ['SDE', 'KLCPD', 'TIRE', 'WIN', 'BINSEG', 'DYNP', 'KERNEL', 'PELT']
   else:
      algorithm_names = ['SDE', 'ClaSP', 'BOCPD', 'KLCPD', 'TIRE', 'WIN', 'BINSEG', 'DYNP', 'KERNEL', 'PELT']
   for corpus_name in results:
      if args.corpus != 'all' and corpus_name != args.corpus:
         continue
      for dataset_name in results[corpus_name]:
         for sde_res, res in zip(sde_results[corpus_name][dataset_name], 
                                 results[corpus_name][dataset_name]):
            if args.dim == 'univariate' and 'ClaSP' not in res: continue
            elif args.dim == 'multivariate' and 'ClaSP' in res: continue
            for alg in res:
               for metric in res[alg]:
                  if metric == 'NAB':
                     for profile in ['Standart', 'LowFN', 'LowFP']:
                        scores[alg][f'NAB.{profile}'].append(res[alg][metric][profile])
                        scores['SDE'][f'NAB.{profile}'].append(sde_res[metric][profile])
                  else:
                      scores[alg][metric].append(res[alg][metric])
                      scores['SDE'][metric].append(sde_res[metric])
   
   for algorithm_name in algorithm_names:
        if args.uncertainty:
           rows.append([algorithm_name] + [f'{np.mean(v):.2f} ± {np.std(v):.2f}' for m in metric_names for v in [scores[algorithm_name][m]]])
        else:
           rows.append([algorithm_name] + [f'{np.mean(scores[algorithm_name][m]):.2f}' for m in metric_names])
   table = tabulate(rows, headers=headers, tablefmt=args.table_format)
   print(table)
elif args.aggregation == 'corpus':
   for corpus_name in results:
      raise NotImplemented
else:
   metric_names = ['NAB.Standart', 'NAB.LowFP', 'NAB.LowFN', 'F1', 'Covering', 'RCPD']
   headers = ['Algorithm'] + metric_names
   scores = defaultdict(lambda: defaultdict(lambda: defaultdict(list))) # dataset_name -> metric_name -> algorithm_name -> metric_scores
   if args.dim == 'multivariate':
      algorithm_names = ['SDE', 'KLCPD', 'TIRE', 'WIN', 'BINSEG', 'DYNP', 'KERNEL', 'PELT']
   else:
      algorithm_names = ['SDE', 'ClaSP', 'BOCPD', 'KLCPD', 'TIRE', 'WIN', 'BINSEG', 'DYNP', 'KERNEL', 'PELT']
   for corpus_name in results:
      if args.corpus != 'all' and corpus_name != args.corpus:
         continue
      for dataset_name in results[corpus_name]:
         for sde_res, res in zip(sde_results[corpus_name][dataset_name],
             results[corpus_name][dataset_name]):
            if args.dim == 'univariate' and 'ClaSP' not in res: continue # dataset is not univariate (only univariate datasets contains ClaSP)
            elif args.dim == 'multivariate' and 'ClaSP' in res: continue # dataset is univariate
            for alg in res:
               for metric in res[alg]:
                  if metric == 'NAB':
                     for profile in ['Standart', 'LowFN', 'LowFP']:
                        scores[dataset_name][f'NAB.{profile}'][alg].append(res[alg][metric][profile])
                        scores[dataset_name][f'NAB.{profile}']['SDE'].append(sde_res[metric][profile])
                        
                  else:
                      scores[dataset_name][metric][alg].append(res[alg][metric])
                      scores[dataset_name][metric]['SDE'].append(sde_res[metric])         
   
   for metric_name in metric_names:
      print('******************************************')
      print(metric_name)
      print('******************************************')
      rows = []
      headers = ['Dataset name'] + [f'{algorithm_name}' for algorithm_name in algorithm_names]
      for dataset_name in scores:
         dname = SYNTHETIC_DATASETS_DESC.get(dataset_name, dataset_name)
         if dname == 'synthetic_dataset_8_sample_11': continue
         if args.uncertainty:
            rows.append([dname] + [f'{np.mean(v):.2f} ± {np.std(v):.2f}' for algorithm_name in algorithm_names for v in [scores[dataset_name][metric_name][algorithm_name]]])
         else:
            rows.append([dname] + [f'{np.mean(v):.2f}' for algorithm_name in algorithm_names for v in [scores[dataset_name][metric_name][algorithm_name]]])

         table = tabulate(rows, headers=headers, tablefmt=args.table_format)
      print(table)



