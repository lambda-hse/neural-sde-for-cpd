#!/bin/bash

N_THREADS=4
for thread in $(seq 1 $N_THREADS); do
    sbatch --gpus=1 --cpus-per-task 11 --error="sde_slurm_logs/$thread.err" --output="sde_slurm_logs/$thread.out" --job-name="CPD-$thread" train_sde_all.sh "$@" &
done

wait
