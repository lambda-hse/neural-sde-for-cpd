import math
import os
import random
from typing import Optional, Union

import numpy as np
import torch
from torch import distributions, nn, optim

import torchsde

sdeint_fn = torchsde.sdeint_adjoint


def _stable_division(a, b, epsilon=1e-7):
    b = torch.where(b.abs().detach() > epsilon, b, torch.full_like(b, fill_value=epsilon) * b.sign())
    return a / b


class LatentSDE(torchsde.SDEStratonovich):

    def __init__(self, D, theta=1.0, mu=0.0, sigma=1., dt=1e-2, adaptive=False, atol=1e-3, rtol=1e-3, n_pos_encodings=1):
        super(LatentSDE, self).__init__(noise_type="diagonal")
        self.D, self.n_pos_encodings, self.dt, self.adaptive, self.atol, self.rtol = D, n_pos_encodings, dt, adaptive, atol, rtol
        logvar = math.log(sigma ** 2 / (2. * theta))

        # Prior drift.
        self.register_buffer("theta", torch.tensor([[theta]]))
        self.register_buffer("mu", torch.tensor([[mu]]))
        self.register_buffer("sigma", torch.tensor([[sigma]]))

        # p(y0).
        self.register_buffer("py0_mean", torch.tensor([[mu]]))
        self.register_buffer("py0_logvar", torch.tensor([[logvar]]))

        # Approximate posterior drift: Takes in 2 positional encodings and the state.
        self.net = nn.Sequential(
            nn.Linear(2*n_pos_encodings+D, 200),
            nn.Tanh(),
            nn.Linear(200, 200),
            nn.Tanh(),
            nn.Linear(200, D)
        )
        # Initialization trick from Glow.
        self.net[-1].weight.data.fill_(0.)
        self.net[-1].bias.data.fill_(0.)

        # q(y0).
        self.qy0_mean = nn.Parameter(torch.tensor([[mu]]), requires_grad=True)
        self.qy0_logvar = nn.Parameter(torch.tensor([[logvar]]), requires_grad=True)

    def f(self, t, y):  # Approximate posterior drift.
        if t.dim() == 0:
            t = torch.full_like(y[:, 0], fill_value=t).unsqueeze(-1)
        # Positional encoding in transformers for time-inhomogeneous posterior.
        input_ = []
        for i in range(self.n_pos_encodings):
            input_.append(torch.sin((i+1)*t))
            input_.append(torch.cos((i+1)*t))
        input_.append(y)
        input_ = torch.cat(input_, dim=-1)
        return self.net(input_)

    def g(self, t, y):  # Shared diffusion.
        return self.sigma.repeat(y.size(0), 1)

    def h(self, t, y):  # Prior drift.
        return self.theta * (self.mu - y)

    def f_aug(self, t, y):  # Drift for augmented dynamics with logqp term.
        y = y[:, 0:self.D]
        f, g, h = self.f(t, y), self.g(t, y), self.h(t, y)
        u = _stable_division(f - h, g)
        f_logqp = .5 * (u ** 2).sum(dim=1, keepdim=True)
        return torch.cat([f, f_logqp], dim=1)

    def g_aug(self, t, y):  # Diffusion for augmented dynamics with logqp term.
        y = y[:, 0:self.D]
        g = self.g(t, y)
        g_logqp = torch.zeros_like(y)
        return torch.cat([g, g_logqp], dim=1)

    def forward(self, ts, batch_size, y0=None, eps=None):
        eps = torch.randn(batch_size, self.D).to(self.qy0_std) if eps is None else eps
        y0_ = self.qy0_mean + eps * self.qy0_std
        if y0 is not None:
            y0_ += y0
        qy0 = distributions.Normal(loc=self.qy0_mean, scale=self.qy0_std)
        py0 = distributions.Normal(loc=self.py0_mean, scale=self.py0_std)
        logqp0 = distributions.kl_divergence(qy0, py0).sum(dim=1)  # KL(t=0).

        aug_y0 = torch.cat([y0_, torch.zeros(batch_size, 1).to(y0_)], dim=1)
        aug_ys = sdeint_fn(
            sde=self,
            y0=aug_y0,
            ts=ts,
            method='reversible_heun',
            adjoint_method='adjoint_reversible_heun',
            dt=self.dt,
            adaptive=self.adaptive,
            rtol=self.rtol,
            atol=self.atol,
            names={'drift': 'f_aug', 'diffusion': 'g_aug'}
        )
        assert(aug_ys.size(2) == self.D+1)
        ys, logqp_path = aug_ys[:, :, 0:self.D], aug_ys[-1, :, -1]
        logqp = (logqp0 + logqp_path).mean(dim=0)  # KL(t=0) + KL(path).
        return ys, logqp

    def sample_p(self, ts, batch_size, eps=None, bm=None):
        eps = torch.randn(batch_size, 1).to(self.py0_mean) if eps is None else eps
        y0 = self.py0_mean + eps * self.py0_std
        return sdeint_fn(self, y0, ts, bm=bm, method='srk', dt=self.dt, names={'drift': 'h'})

    def sample_q(self, ts, batch_size, eps=None, bm=None):
        eps = torch.randn(batch_size, 1).to(self.qy0_mean) if eps is None else eps
        y0 = self.qy0_mean + eps * self.qy0_std
        return sdeint_fn(self, y0, ts, bm=bm, method='srk', dt=self.dt)

    @property
    def py0_std(self):
        return torch.exp(.5 * self.py0_logvar)

    @property
    def qy0_std(self):
        return torch.exp(.5 * self.qy0_logvar)
    
